<?php
include 'src/autoloader.php';
/**
 * So, we have a simple lift with max of 650 kg capacity, from first to ninth floor
 */
$lift = (new \SixthSystems\Lift\ConcreteLiftFactory())
    ->withController(new \SixthSystems\Controllers\SimpleController(650, 1, 9))
    ->withCabin(new \SixthSystems\Cabins\TwoDoorCabin())
    ->get();

$controlCenter = new \SixthSystems\ControlCenter\ControlCenter();
$controlCenter->addLift($lift);

/**
 * So, lets think that we have example user, that weights 70 kg, and need to go from first to fifth floor
 */
$user = new \SixthSystems\User\ExampleUser($lift, 70, 1, 5);

/**
 * First, user should call the lift
 */
$user->call();

/**
 * As we don't need to much specification right now, let's think that any lift action is executes immediately,
 * so we don't need to emulate user waiting
 */
$user->enter();

/**
 * User already know, which floor he want, so we don't need to provide this info. Simply press floor button, that he needs
 */
$user->pressFloorButton();

/**
 * Cool, we are here now, can exit
 */
$user->leave();

$controlCenter->liftsState();