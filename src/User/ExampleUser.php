<?php

namespace SixthSystems\User;

use SixthSystems\Lift\Lift;
use SixthSystems\Loggers\HtmlLogger;

/**
 * Class ExampleUser is example to show, how we can work with lift
 * @package SixthSystems\User
 */
class ExampleUser
{
    use HtmlLogger;

    /**
     * @var Lift
     */
    protected $lift;

    protected $weight;
    protected $atFloor;
    protected $toFloor;

    /**
     * ExampleUser constructor.
     * @param Lift $lift
     * @param int $weight
     * @param int $atFloor
     * @param int $toFloor
     */
    public function __construct($lift, $weight, $atFloor = 1, $toFloor = 9)
    {
        $this->lift = $lift;
        $this->weight = $weight;
        $this->atFloor = $atFloor;
        $this->toFloor = $toFloor;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function call()
    {
        $this->log('User called lift at ' . $this->atFloor . ' floor');
        $this->lift->moveTo($this->atFloor);
    }

    public function enter()
    {
        $this->log('User entered lift at ' . $this->atFloor . ' floor');
        $this->lift->userEnter($this->getWeight());
    }

    public function pressFloorButton()
    {
        $this->log('User press button with ' . $this->toFloor . ' floor');
        $this->lift->moveTo($this->toFloor);
    }

    public function leave()
    {
        $this->log('User leave lift at ' . $this->toFloor . ' floor');
        $this->lift->userLeave($this->getWeight());
    }
}