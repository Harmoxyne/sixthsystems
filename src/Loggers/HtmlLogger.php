<?php

namespace SixthSystems\Loggers;


trait HtmlLogger
{
    public function log($text)
    {
        echo '<p>', $text, '</p>';
    }
}