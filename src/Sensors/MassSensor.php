<?php

namespace SixthSystems\Sensors;


class MassSensor implements Sensor
{
    protected $currentWeight;
    protected $maxWeight;

    /**
     * MassSensor constructor.
     * @param int $maxWeight
     */
    public function __construct($maxWeight)
    {
        $this->maxWeight = $maxWeight;
        $this->currentWeight = 0;
    }

    /**
     * @param int $weight
     */
    public function addWeight($weight)
    {
        $this->currentWeight += $weight;
    }

    /**
     * @param int $weight
     */
    public function removeWeight($weight)
    {
        $this->currentWeight -= $weight;
    }

    /**
     * @return string
     */
    public function getState()
    {
        if ($this->currentWeight > $this->maxWeight) {
            return "Overweight";
        }
        return "OK";
    }
}