<?php

namespace SixthSystems\Sensors;


interface Sensor
{
    /**
     * @return string
     */
    public function getState();
}