<?php

namespace SixthSystems\Lift;


use SixthSystems\Cabins\Cabin;
use SixthSystems\Controllers\Controller;
use SixthSystems\Loggers\HtmlLogger;

class ConcreteLift implements Lift
{
    use HtmlLogger;
    /**
     * @var Cabin
     */
    protected $cabin;

    /**
     * @var Controller
     */
    protected $controller;

    /**
     * @var int
     */
    protected $maxCapacity;

    /**
     * @var bool
     */
    protected $isMoving;

    /**
     * ConcreteLift constructor.
     * @param Cabin $cabin
     * @param Controller $controller
     * @param int $maxCapacity
     */
    public function __construct($cabin, $controller, $maxCapacity)
    {
        $this->cabin = $cabin;
        $this->controller = $controller;
        $this->maxCapacity = $maxCapacity;

        $this->log('New lift created');
    }

    /**
     * @param int $toFloor
     * @throws \Exception
     */
    public function moveTo($toFloor)
    {
        if ($toFloor == $this->getCurrentFloor()) {
            $this->log('Already here');
            return;
        }

        $this->log("Trying to move to $toFloor from {$this->getCurrentFloor()}");
        if ($this->controller->getMassSensorState() != "OK") {
            $this->log("Oops, overweight");
            throw new \Exception("Overweight");
        }
        $this->cabin->closeDoor();
        $this->isMoving = true;
        $this->controller->moveTo($toFloor);
        $this->isMoving = false;
        $this->cabin->openDoor();
        $this->log("On $toFloor now");
    }

    /**
     * @return int
     */
    public function getCurrentFloor()
    {
        return $this->controller->getCurrentFloor();
    }

    /**
     * @return bool
     */
    public function isMoving()
    {
        return $this->isMoving;
    }

    /**
     * @param int $userWeight
     */
    public function userEnter($userWeight)
    {
        $this->controller->addWeight($userWeight);
        $this->log("User with weight of $userWeight come in");
    }

    /**
     * @param int $userWeight
     */
    public function userLeave($userWeight)
    {
        $this->controller->removeWeight($userWeight);

        $this->log("User with weight of $userWeight come out");

    }
}