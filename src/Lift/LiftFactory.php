<?php

namespace SixthSystems\Lift;


interface LiftFactory
{
    /**
     * @return Lift
     */
    public function get();
}