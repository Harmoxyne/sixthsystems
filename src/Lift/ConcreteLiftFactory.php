<?php

namespace SixthSystems\Lift;


use SixthSystems\Cabins\Cabin;
use SixthSystems\Controllers\Controller;

class ConcreteLiftFactory implements LiftFactory
{
    protected $controller = null;
    protected $cabin = null;
    protected $maxCapacity = 650;

    /**
     * @param Cabin $cabin
     * @return $this
     */
    public function withCabin($cabin)
    {
        $this->cabin = $cabin;
        return $this;
    }

    /**
     * @param Controller $controller
     * @return $this
     */
    public function withController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @param int $maxCapacity
     * @return $this
     */
    public function withMaxCapacity($maxCapacity)
    {
        $this->maxCapacity = $maxCapacity;
        return $this;
    }

    /**
     * @return ConcreteLift
     */
    public function get()
    {
        return new ConcreteLift($this->cabin, $this->controller, $this->maxCapacity);
    }
}