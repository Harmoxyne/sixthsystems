<?php

namespace SixthSystems\Lift;


interface Lift
{
    /** Calls lift to specific floor
     * @param int $toFloor
     * @return void
     *
     * @throws \Exception
     */
    public function moveTo($toFloor);

    /**
     * @param int $weight
     * @return void
     */
    public function userEnter($weight);

    /**
     * @param int $weight
     * @return void
     */
    public function userLeave($weight);

    /**
     * @return int
     */
    public function getCurrentFloor();

    /**
     * @return bool
     */
    public function isMoving();
}