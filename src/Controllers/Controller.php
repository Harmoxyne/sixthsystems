<?php

namespace SixthSystems\Controllers;

/**
 * This class controls where is lift now, how much weight it have and tells it to start or stop moving
 * Class Controller
 * @package SixthSystems\Controllers
 */
abstract class Controller
{
    /**
     * @param int $toFloor
     * @return void
     *
     * @throws \Exception
     */
    public abstract function moveTo($toFloor);

    /**
     * @return int
     */
    public abstract function getCurrentFloor();

    /**
     * @return string
     */
    public abstract function getMassSensorState();

    /**
     * @param int $weight
     * @return void
     */
    public abstract function addWeight($weight);

    /**
     * @param int $weight
     * @return void
     */
    public abstract function removeWeight($weight);
}