<?php

namespace SixthSystems\Controllers;

/**
 * Special implementation to make lifts, that moving from one floor to another all the time
 * Class ShabbatController
 * @package SixthSystems\Controllers
 */
class ShabbatController extends Controller
{

    public function __construct($maxWeight, $minFloor, $maxFloor)
    {
        // TODO: Implement __construct() method.

        /**
         * Could be something like this, but should have threading, I think
         */
        /*
         while(true) {
            if ($this->getCurrentFloor() >= $maxFloor) {
                $vector = -1;
            } elseif ($this->getCurrentFloor() <= $minFloor){
                $vector = 1;
            }
            $this->moveTo($this->getCurrentFloor + vector);
        }
         */
    }

    public function moveTo($toFloor)
    {
        // TODO: Implement moveTo() method.
    }

    /**
     * @return int
     */
    public function getCurrentFloor()
    {
        // TODO: Implement getCurrentFloor() method.
    }

    /**
     * @return string
     */
    public function getMassSensorState()
    {
        // TODO: Implement getMassSensorState() method.
    }

    /**
     * @param int $weight
     * @return void
     */
    public function addWeight($weight)
    {
        // TODO: Implement addWeight() method.
    }

    /**
     * @param int $weight
     * @return void
     */
    public function removeWeight($weight)
    {
        // TODO: Implement removeWeight() method.
    }
}