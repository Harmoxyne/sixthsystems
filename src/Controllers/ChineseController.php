<?php

namespace SixthSystems\Controllers;

/**
 * Special implementation to make lifts, that except some floors
 * Class ChineseController
 * @package SixthSystems\Controllers
 */
class ChineseController extends Controller
{

    /**
     * ChineseController constructor.
     * @param int $maxWeight
     * @param array $floorsRange
     */
    public function __construct($maxWeight, $floorsRange)
    {
        //TODO: Implement __construct() method.
    }

    public function moveTo($toFloor)
    {
        // TODO: Implement moveTo() method.
    }

    /**
     * @return int
     */
    public function getCurrentFloor()
    {
        // TODO: Implement getCurrentFloor() method.
    }

    /**
     * @return string
     */
    public function getMassSensorState()
    {
        // TODO: Implement getMassSensorState() method.
    }

    /**
     * @param int $weight
     * @return void
     */
    public function addWeight($weight)
    {
        // TODO: Implement addWeight() method.
    }

    /**
     * @param int $weight
     * @return void
     */
    public function removeWeight($weight)
    {
        // TODO: Implement removeWeight() method.
    }
}