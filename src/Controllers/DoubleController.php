<?php

namespace SixthSystems\Controllers;

/**
 * Special implementation to make lift with two cabins but same controls
 * Class DoubleController
 * @package SixthSystems\Controllers
 */
class DoubleController extends Controller
{

    public function moveTo($toFloor)
    {
        // TODO: Implement moveTo() method.
    }

    /**
     * @return int
     */
    public function getCurrentFloor()
    {
        // TODO: Implement getCurrentFloor() method.
    }

    /**
     * @return string
     */
    public function getMassSensorState()
    {
        // TODO: Implement getMassSensorState() method.
    }

    /**
     * @param int $weight
     * @return void
     */
    public function addWeight($weight)
    {
        // TODO: Implement addWeight() method.
    }

    /**
     * @param int $weight
     * @return void
     */
    public function removeWeight($weight)
    {
        // TODO: Implement removeWeight() method.
    }
}