<?php

namespace SixthSystems\Controllers;


use SixthSystems\Loggers\HtmlLogger;
use SixthSystems\Sensors\MassSensor;

/**
 * Simple implementation for lift, that have one cabin and plain range of floors
 * Class SimpleController
 * @package SixthSystems\Controllers
 */
class SimpleController extends Controller
{

    use HtmlLogger;

    protected $currentFloor;
    protected $minFloor;
    protected $maxFloor;

    protected $maxWeight;
    protected $massSensor;

    /**
     * SimpleController constructor.
     * @param int $maxWeight
     * @param int $minFloor
     * @param int $maxFloor
     */
    public function __construct($maxWeight, $minFloor, $maxFloor)
    {
        $this->maxWeight = $maxWeight;
        $this->minFloor = $minFloor;
        $this->maxFloor = $maxFloor;

        $this->currentFloor = $this->minFloor;

        $this->massSensor = new MassSensor($this->maxWeight);
    }

    /**
     * @return int
     */
    public function getCurrentFloor()
    {
        return $this->currentFloor;
    }

    /**
     * @return int
     */
    public function getMinFloor()
    {
        return $this->minFloor;
    }

    /**
     * @return int
     */
    public function getMaxFloor()
    {
        return $this->maxFloor;
    }

    /**
     * @return int
     */
    public function getMaxWeight()
    {
        return $this->maxWeight;
    }

    /**
     * @return string
     */
    public function getMassSensorState()
    {
        return $this->massSensor->getState();
    }

    /**
     * @param int $toFloor
     * @throws \Exception
     */
    public function moveTo($toFloor)
    {
        if ($toFloor < $this->getMinFloor() || $toFloor > $this->getMaxFloor()) {
            throw new \Exception("No such floor");
        }
        $this->log("Moving to $toFloor floor");
        $this->currentFloor = $toFloor;
    }

    /**
     * @param int $weight
     * @return void
     */
    public function addWeight($weight)
    {
        $this->massSensor->addWeight($weight);
    }

    /**
     * @param int $weight
     * @return void
     */
    public function removeWeight($weight)
    {
        $this->massSensor->removeWeight($weight);
    }
}