<?php

namespace SixthSystems\ControlCenter;

use SixthSystems\Lift\Lift;
use SixthSystems\Loggers\HtmlLogger;

/**
 * Class ControlCenter - center to control lifts
 * Lets think, that this class is working in another thread, so he will receive any info immediately, not at the end of lift control function
 * @package SixthSystems\ControlCenter
 */
class ControlCenter
{
    use HtmlLogger;
    /**
     * @var array
     */
    protected $lifts = [];

    /**
     * @param Lift $lift
     */
    public function addLift($lift)
    {
        $this->lifts[] = $lift;
    }

    /**
     * @return void
     */
    public function liftsState()
    {
        foreach ($this->lifts as $lift) {
            $this->showLiftInfo($lift);
        }
    }

    /**
     * @param Lift $lift
     * @return void
     */
    private function showLiftInfo($lift)
    {
        $this->log("Moving: " . ($lift->isMoving() ? "Yes" : "No") . "; At floor: {$lift->getCurrentFloor()}");
    }
}