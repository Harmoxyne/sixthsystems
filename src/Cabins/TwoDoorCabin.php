<?php

namespace SixthSystems\Cabins;


use SixthSystems\Loggers\HtmlLogger;

/**
 * Class TwoDoorCabin - simple cabin with two doors, that are closed at a time
 * @package SixthSystems\Cabins
 */
class TwoDoorCabin implements Cabin
{
    use HtmlLogger;

    protected $firstDoorOpened;
    protected $secondDoorOpened;

    public function openDoor()
    {
        $this->log('Opening first door');
        $this->firstDoorOpened = true;

        $this->log('Opening second door');
        $this->secondDoorOpened = true;
    }

    public function closeDoor()
    {
        $this->log('Closing first door');
        $this->firstDoorOpened = false;

        $this->log('Closing second door');
        $this->secondDoorOpened = false;
    }

    /**
     * @return bool
     */
    public function isOpen()
    {
        return $this->firstDoorOpened && $this->secondDoorOpened;
    }
}