<?php

namespace SixthSystems\Cabins;


use SixthSystems\Loggers\HtmlLogger;

/**
 * Class OneDoorCabin - simple cabin with one door.
 * @package SixthSystems\Cabins
 */
class OneDoorCabin implements Cabin
{
    use HtmlLogger;

    /**
     * @var bool
     */
    protected $opened;

    /**
     * @return void
     */
    public function openDoor()
    {
        $this->opened = true;

        $this->log('Opening door');
    }

    /**
     * @return void
     */
    public function closeDoor()
    {
        $this->opened = false;

        $this->log('Closing door');
    }

    /**
     * @return bool
     */
    public function isOpen()
    {
        return $this->opened;
    }
}