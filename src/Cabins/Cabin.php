<?php

namespace SixthSystems\Cabins;

/**
 * Interface Cabin
 * @package SixthSystems\Cabins
 */
interface Cabin
{
    /**
     * @return void
     */
    public function openDoor();

    /**
     * @return void
     */
    public function closeDoor();

    /**
     * @return bool
     */
    public function isOpen();
}